<?php

class VMJsonServiceResponseBuilder extends CComponent
{
	/**
	 * @param null $data
	 */
	public static function respond($data = null)
	{
		self::build($data, null, VMServiceResponseCode::NO_ERROR);
	}

	/**
	 * @param $data
	 * @param $exceptions
	 * @param $code
	 */
	public static function build($data, $exceptions, $code)
	{
		$result = array('result' => array(
			'succeeded' => $exceptions == null,
			'exceptions' => $exceptions,
			'code' => $code
		));

		if ($data == null)
			$data = array();

		$response = array_merge($result, $data);
		$final = json_encode(array('response' => $response));

		header('Content-type: application/json');

		echo $final;

		Yii::app()->end();
	}

	/**
	 * @param     $mixed
	 * @param int $code
	 */
	public static function respondWithError($mixed, $code = VMServiceResponseCode::SERVICE_ERROR)
	{
		$message = array();
		if (is_string($mixed)) {
			$message[] = array(
                'attribute' => NULL,
                'message' => $mixed
            );
		} else if (is_array($mixed)) {
			foreach ($mixed as $attribute => $errors) {
				$message[] = array(
					'attribute' => is_string($attribute) ? $attribute : null,
					'message' => is_array($errors) ? implode(', ', $errors) : $errors,
				);
			}
		}

		self::build(array(), $message, $code);
	}
}