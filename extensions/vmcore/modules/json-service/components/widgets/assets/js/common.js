/**
 * Created with JetBrains PhpStorm.
 * User: egor
 * Date: 22.01.14
 * Time: 14:15
 * To change this template use File | Settings | File Templates.
 */
/*jsHint*/
/*global window*/
(function ($) {
    'use strict';
    var sendRequest = function() {
        var requestBlock = '#request-' + $(this).attr('id');
        var responseBlock = '#response-' + $(this).attr('id');
        var data = {request: JSON.parse($(requestBlock).text())};

        $.ajax({
            type: 'POST',
            context: $(responseBlock),
            url: $(this).data('url'),
            data: JSON.stringify(data),

            /**@this {jQuery}*/
            success: function (response, status, xhr) {
                this.parent().removeClass('hidden');

                var FORMAT_TAB_SIZE = 4;
                var regExp = /.*\/json/;

                if (regExp.test(xhr.getResponseHeader('content-type'))) {
                    response = JSON.stringify(response, null, FORMAT_TAB_SIZE);
                    this.removeClass('non-pre');
                } else {
                    this.addClass('non-pre');
                }

                this.html(response);
            },
            error: function (response) {
                this.parent().removeClass('hidden');
                this.html(response.responseText);
            }
        });
    };

    $(window.document).ready(function () {
        $('.remove-node-btn').on('click',function(){
            var parent = $(this).parent();
            var prev = parent.prev('.node');
            var next = parent.next('.node');
            if(prev.length && !next.length){
                prev.children('.comma').remove();
            }
            parent.remove();
        });
        $('.remove-node-btn').popover({placement:'left',trigger:'hover'});
        $('.request-btn').on('click', sendRequest);
    });
})(window.jQuery);

//@ sourceURL= /documentation/js/common.js