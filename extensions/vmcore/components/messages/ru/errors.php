<?php

return array(
    '{property} is not set up properly' => '{property} не корректно',
    'There are no parameters matching "{className}"' => 'Не существует параметров соответствующих "{className}"',
);