<?php

class VMAssetPublisher extends CComponent
{
	public $cssFolder = 'css';
	public $jsFolder = 'js';
	protected $assetsUrl = null;

	public function publish($path)
	{
		$this->assetsUrl = Yii::app()->assetManager->publish($path, true, -1, YII_DEBUG);

		$this->registerCss($path);
		$this->registerScripts($path);
	}

	protected function registerCss($path)
	{
		$files = $this->findFiles($path . DIRECTORY_SEPARATOR . $this->cssFolder, 'css');

		foreach ($files as $file) {
			$pathInfo = pathinfo($file);
			Yii::app()->clientScript->registerCssFile($this->assetsUrl . DIRECTORY_SEPARATOR . $this->cssFolder . DIRECTORY_SEPARATOR . $pathInfo['basename']);
		}
	}

	protected function findFiles($path, $ext)
	{
		return CFileHelper::findFiles($path, array(
			'fileTypes' => array($ext),
			'level' => -1
		));
	}

	protected function registerScripts($path)
	{
		$files = $this->findFiles($path . DIRECTORY_SEPARATOR . $this->jsFolder, 'js');
		foreach ($files as $file) {
			$pathInfo = pathinfo($file);
			Yii::app()->clientScript->registerScriptFile($this->assetsUrl . DIRECTORY_SEPARATOR . $this->jsFolder . DIRECTORY_SEPARATOR . $pathInfo['basename']);
		}
	}
}