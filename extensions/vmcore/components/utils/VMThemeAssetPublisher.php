<?php

class VMThemeAssetPublisher extends CApplicationComponent
{
	public $sharedFolder = 'shared';
	public $includeCoreAssets = false;

	private $corePath = null;
	private $sharedPath = null;
	private $themePath = null;
	private $publisher = null;

	public function init()
	{
		$this->publisher = new VMAssetPublisher();

		if($this->includeCoreAssets) {
			$this->corePath = $this->publishFolder('assets', Yii::getPathOfAlias('yiivm.extensions.vmcore'));
		}

		$this->sharedPath = $this->publishFolder($this->sharedFolder . DIRECTORY_SEPARATOR . 'assets');
		$this->themePath = $this->publishFolder(Yii::app()->theme->name . DIRECTORY_SEPARATOR . 'assets');
	}

	private function publishFolder($folder, $basePath = null)
	{
		if(!$basePath) {
			$basePath = Yii::app()->themeManager->basePath;
		}

		$path = $basePath . DIRECTORY_SEPARATOR . $folder;

		if (file_exists($path) && is_dir($path)) {
			$this->publisher->publish($path);
		}

		return $path;
	}

	public function getSharedUrl()
	{
		return Yii::app()->assetManager->getPublishedUrl($this->sharedPath);
	}

	public function getThemeUrl()
	{
		return Yii::app()->assetManager->getPublishedUrl($this->themePath);
	}

	public function getCoreUrl()
	{
		return Yii::app()->assetManager->getPublishedUrl($this->corePath);
	}

	public function publishAll() {
		
	}
}