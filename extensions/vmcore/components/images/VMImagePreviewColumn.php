<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Alex
 * Date: 26.07.13
 * Time: 2:30 PM
 * To change this template use File | Settings | File Templates.
 */

Yii::import('bootstrap.widgets.TbImageColumn');

class VMImagePreviewColumn extends TbImageColumn
{

	// Default thumbnail width
	public $thumbnail = 150;

	// Default title
	public $headerTitle = 'Image';

	// Detault name value
	public $name = 'image_filename';
	public $isUrl = false;

	public function init()
	{
		parent::init();

		$this->usePlaceKitten = false;
		$this->usePlaceHoldIt = true;
		$this->placeHoldItSize = $this->thumbnail . 'x' . $this->thumbnail;

		$this->imageOptions = array('width' => $this->thumbnail);
		$this->header = $this->headerTitle;
		$this->headerHtmlOptions = array('class' => 'span2');

		if (!$this->isUrl) {
			$this->imagePathExpression = '$data->{$this->name} != null ? Yii::app()->baseUrl. DIRECTORY_SEPARATOR .$data->{$this->name} : null';
		} else {
			$this->imagePathExpression = '$data->{$this->name} != null ? $data->{$this->name} : null';
		}
	}
}