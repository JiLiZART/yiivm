<?php

class VMCore extends CApplicationComponent
{
	public $checkVersion = false;

	public function init()
	{
		Yii::import('vmcore.components.auth.*');
		Yii::import('vmcore.components.data.*');
		Yii::import('vmcore.components.docs.*');
		Yii::import('vmcore.components.email.*');
		Yii::import('vmcore.components.geo.*');
		Yii::import('vmcore.components.images.*');
		Yii::import('vmcore.components.media.*');
		Yii::import('vmcore.components.payments.*');
		Yii::import('vmcore.components.pushes.*');
		Yii::import('vmcore.components.service.*');
		Yii::import('vmcore.components.ui.*');
		Yii::import('vmcore.components.utils.*');
		Yii::import('vmcore.components.config.*');
		Yii::import('vmcore.components.config.modifiers.*');
		Yii::import('vmcore.components.log.*');
		Yii::import('vmcore.widgets.*');

		parent::init();
	}

	public function getVersion()
	{
		return '1.0';
	}
}