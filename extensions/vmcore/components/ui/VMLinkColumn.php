<?php


class VMLinkColumn extends TbDataColumn
{
	public $label = null;
	public $options = array();

	protected function renderDataCellContent($row, $data)
	{
		$this->options = (object)$this->options;

		if ($this->value !== null) {
			$value = $this->evaluateExpression($this->value, array('data' => $data, 'row' => $row));
		} elseif ($this->name !== null) {
			$value = CHtml::value($data, $this->name);
		}

		if ($value === null && isset($this->options->label)) {
			$value = $this->options->label;
		}

		$content = $value === null ? $this->grid->nullDisplay : $this->grid->getFormatter()->format($value, $this->type);
		$realUrl = $value;

		echo CHtml::tag('a', array('href' => $realUrl, 'target' => '_blank'), $content);
	}
}