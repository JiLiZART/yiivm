<?php
/**
 * @class VMDependedOptionsLoad
 * Description of VMDependedOptionsLoad class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMDependedOptionsLoadAction extends CAction {
	public $modelName = null;
	public $requestModel = null;
	public $attributeName = null;

	public $value = 'id';
	public $name  = 'name';

	/**
	 * @var CDbCriteria|null $criteria
	 */
	public $criteria = null;

	public function run() {
		if(!$this->modelName) {
			throw new CHttpException(500, Yii::t('vmcore.errors', '{property} is not set up properly', array('{property}' => 'modelName')));
		}

		if (!$this->requestModel) {
			throw new CHttpException(500, Yii::t('vmcore.errors', '{property} is not set up properly', array('{property}' => 'requestModel')));
		}

		if (!$this->attributeName) {
			throw new CHttpException(500, Yii::t('vmcore.errors', '{property} is not set up properly', array('{property}' => 'attributeName')));
		}

		$model = CActiveRecord::model($this->requestModel);
		if(!$model) {
			throw new CHttpException(500, Yii::t('vmcore.ui', '{className} not found', array('{className}' => $this->requestModel)));
		}

		if(!VMEntitySaver::containsEntity($this->requestModel)) {
			throw new CHttpException(400, Yii::t('vmcore.errors', 'There are no parameters matching "{className}"', array(
				'{className}' => $this->requestModel,
			)));
		}

		$attributes = (object) Yii::app()->request->getParam($this->requestModel);

		$criteria = new CDbCriteria();
		$criteria->compare($this->attributeName, $attributes->{$this->attributeName});

		if($this->criteria) {
			$criteria->mergeWith($this->criteria);
		}

		$entityModel = CActiveRecord::model($this->modelName);
		if(!$entityModel) {
			throw new CHttpException(500, Yii::t('vmcore.ui', '{className} not found', array('{className}' => $this->modelName)));
		}

		$entities = $entityModel->findAll($criteria);

		if($entities) {
			foreach ($entities as $entity) {
				echo CHtml::tag('option', array(
					'value' => $entity->{$this->value}
				), $entity->{$this->name});
			}
		}

		Yii::app()->end();
	}
} 