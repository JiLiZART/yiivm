<?php
/**
 * @class VMCreditCardType
 * Description of VMCreditCardType class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMCreditCardType extends VMEnum {
	const TYPE_VISA = 'VISA';
	const TYPE_MASTERCARD = 'MasterCard';

	protected static function meta() {
		return array(
			self::TYPE_VISA => array(
				'name' => 'VISA',
				'formatted' => 'VISA',
			),
			self::TYPE_MASTERCARD => array(
				'name' => 'MasterCard',
				'formatted' => 'MC',
			)
		);
	}

	public static function getTypes() {
		return array(
			self::TYPE_VISA,
			self::TYPE_MASTERCARD,
		);
	}
} 