<?php

class VMPassGenerator extends CComponent
{
	private $passFilename = 'pass.json';

	public function create(VMPass $pass, $path, VMPassCertificates $certificates, $files = array())
	{
		$zip = new ZipArchive();
		if ($zip->open($path, ZipArchive::CREATE | ZipArchive::OVERWRITE) !== true) {
			return false;
		}

		$passContent = CJSON::encode(VMEntityConverter::json($pass));
		$zip->addFromString($this->passFilename, $passContent);

		foreach ($files as $file) {

			$pathInfo = (object)pathinfo($file);
			$zip->addFile($file, $pathInfo->basename);
		}

		$manifest = $this->createManifest($files, $passContent);
		$zip->addFile($manifest, 'manifest.json');

		$signature = $this->signManifest($manifest, $certificates);
		$zip->addFile($signature, 'signature');

		$zip->close();

		unlink($manifest);
		unlink($signature);
		return true;
	}

	private function createManifest($files, $passContent)
	{
		$results = array();

		$results[$this->passFilename] = sha1($passContent);

		foreach ($files as $file) {
			$pathInfo = (object)pathinfo($file);
			$results[$pathInfo->basename] = sha1_file($file);
		}

		$manifestContent = CJSON::encode($results);
		$tempFile = tempnam(sys_get_temp_dir(), 'manifest');
		file_put_contents($tempFile, $manifestContent);
		return $tempFile;
	}

	private function signManifest($manifest, VMPassCertificates $certificates)
	{
		$tempFile = tempnam(sys_get_temp_dir(), 'signature');
		$command = sprintf('openssl smime -binary -sign -certfile %s -signer %s -inkey %s -in %s -out %s -outform DER -passin pass:%s',
			$certificates->wwdr, $certificates->passCertificate, $certificates->passKey, $manifest, $tempFile, $certificates->password
		);
		shell_exec($command);
		return $tempFile;
	}
}