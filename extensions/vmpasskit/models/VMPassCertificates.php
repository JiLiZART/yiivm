<?php

class VMPassCertificates extends VMModelEntity
{
	public $passKey;
	public $passCertificate;
	public $wwdr;
	public $password;

	public function __construct($key, $certificate, $password, $wwdr)
	{
		$this->passKey = $key;
		$this->passCertificate = $certificate;
		$this->wwdr = $wwdr;
		$this->password = $password;
	}
}