<?php

class VMPass extends VMModelEntity
{
	public $formatVersion = 1;
	public $passTypeIdentifier;
	public $serialNumber;
	public $teamIdentifier;
	public $organizationName;
	public $description;
	public $backgroundColor;
	public $logoText;
	// public $foregroundColor;

	/** @var VMBarcode */
	public $barcode;

}